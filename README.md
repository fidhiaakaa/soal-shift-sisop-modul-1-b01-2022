# soal-shift-sisop-modul-1-B01-2022

ANGGOTA BO1
|             Nama              |       NRP       |
| ---------------------------   | --------------- |
| Fachrendy Zulfikar Abdillah   |    5025201018   |
| Ingwer Ludwig Nommensen       |    5025201      |
| Fidhia Ainun Khofifah         | 05111940000203  |

## Soal 1

Soal 1 meminta kita untuk membuat sebuah program register akun yang disimpan pada sebuah file agar nanti dapat digunakan untuk login. Pada laman login, terdapat beberapa perintah pula yang dapat dijalankan.

## Jawaban 1a

Untuk menyimpan username dan password dari register.sh, data akan disimpan di ./user/user.txt dengan command `mkdir "$folder_location"/users` untuk membuat folder users dan `touch "$folder_location"/users/user.txt` untuk membuat file tempat menyimpan username dan password yang didaftarkan

## Jawaban 1b

Untuk menjaga keamanan input password pada login dan register, password harus hidden saat dimasukkan, yaitu dengan command `read -s`. Lalu untuk kriteria password yang dimasukkan adalah:
1. Minimal 8 karakter
Menggunakan kondisi `if[[ ${#password} -lt 8 ]]`
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
Menggunakan kondisi `if[[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]*]]`
3. Alphanumeric
Menggunakan kondisi `if[["$password" != *[0-9]*]]`
4. Tidak boleh sama dengan username
Menggunakan kondisi `if[[ $password == $username ]]`

## Jawaban 1c
Setiap percobaan login dan register akan tercatat pada log.txt dengan format MM/DD/YY hh:mm:ss yaitu dengan command `$(date +%D)` untuk mendapat tanggal dan `time=$(date +%T)` untuk mendapat waktu. Jika log.txt belum ada, maka file log.txt akan dibuat pada lokasi folder dengan command `touch`.

Message log untuk setiap kriteria adalah sebagai berikut:
1. Ketika mencoba register dengan username yang sudah terdaftar, maka pesan log adalah error user exist
Menggunakan kondisi `if grep -q $username "$user_location"/user.txt`. Lalu message log akan masuk ke log.txt dengan format `$calendar $time REGISTER:ERROR $user_exist >> $folder_location/log.txt`
2. Ketika percobaan register berhasil maka akan menulis pesan bahwa username telah terdaftar
Jika semua kondisi terpenuhi, maka log akan menulis `$calendar $time REGISTER:INFO User $username registered successfully >> "$folder_location"/log.txt` serta username dan password akan disimpan di user.txt dengan `$username $password >> "$user_location"/user.txt`.
3. Ketika user login dengan password yang tidak sesuai maka akan menulis pesan bahwa login gagal pada username tersebut
Jika kondisi bahwa username atau password salah, maka akan menulis log `"$calendar $time LOGIN:ERROR $fail" >> "$log_txt"`
4. Ketika user berhasil login, maka akan menulis pesan bahwa username telah masuk
Jika kondisi bahwa username dan password memenuhi, maka user akan masuk dan menulis log `"$calendar $time LOGIN:INFO User $username logged in" >> "$log_txt"`

## Jawaban 1d

Setelah login, akan terdapat 2 command yang dapat dijalankan, dl dan att.
1. Command dl
Command dl adalah download, dengan argumen N saat input (dl <N>) dengan N merupakan jumlah file yang akan didownload dari link yang disediakan. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME yaitu dengan `"$(date +%Y-%m-%d)__$username"`. File yang didownload memiliki format nama PIC_XX dengan nomor yang berurutan menggunakan command

```shell
for(( i=$count+1; i<=$n+$count; i++ ))
  do
    wget https://loremflickr.com/320/240 -O "$folder"/PIC_$i.jpg
  done
```
Setelah didownload, maka folder akan di zip kan dengan password yang sama dengan user menggunakan command `zip --password $password -r "$folder".zip "$folder"/`. Jika file zip sudah ada (dicek menggunakan grep) maka zip akan diunzip dulu, lalu file download akan ditambahkan ke folder tersebut, lalu dizipkan lagi. Commandnya adalah

```shell
unzip -P $password "$folder.zip"
  rm $folder.zip
  count=$(find $folder -type f | wc -l)
```
2. Command att
Command att adalah mengoutput berapa kali percobaan login yang berhasil maupun gagal. Command att ini melihat data yang ada pada log.txt untuk mengetahui jumlah dari percobaan tersebut. Command yang digunakan adalah
```shell
awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count)}' "$log_txt"
```

## Soal 2

Pada soal 2 diminta untuk menampilkan beberapa request yang didapatkan dari file log `log_website_daffainfo.log`.

### Jawaban Soal 2a

Pada bagian ini, diminta untuk membuat folder bernama forensic_log_website_daffainfo_log. Maka, masukkan perintah `mkdir forensic_log_website_daffainfo_log` pada file script.
```shell
mkdir forensic_log_website_daffainfo_log
```

### Jawaban Soal 2b



### Jawaban Soal 2c

Pada soal 2c diminta untuk menampilkan IP dan jumlah IP yang paling banyak melakukan request. Pertama, kami menggunakan command BEGIN untuk mendeklarasikan field separator (FS) dan mendeklarasikan variabel `max` untuk menyimpan jumlah IP dan `ipmax` untuk menyimpan IP berupa string dengan jumlah request terbanyak.
```shell
awk 'BEGIN {FS=":";max=0;ipmax="";}
```
Selanjutnya, kami membuat action yang berfungsi untuk menyimpan IP ke dalam array ip dan melakukan increment.
```shell
if(NR>1){
    ip[$1]++ #menyimpan IP ke array
  }
```
Kemudian, kami deklarasikan command END, kemudian melakukan pencarian IP dengan jumlah terbanyak di dalam array ip. Lalu, hasilnya dicetak dan dimasukkan ke dalam result.txt.
```shell
END {
  max=0 #set 0 untuk perbandingan
  ipmax=""
  for (b in ip){
    if(ip[b]>max){ #cek tiap IP dengan banyak request
      ipmax=b
      max=ip[b]
    }
  }
  print "IP yang paling banyak mengakses server adalah:", ipmax, "sebanyak", max, "requests\n"
}
 ' log_website_daffainfo.log > "$root"/forensic_log_website_daffainfo_log/result.txt
```
Syntax `ip[b]>max` berfungsi untuk melakukan cek tiap IP di dalam array, apakah jumlahnya lebih banyak dari yang tercatat. Syntax `ipmax=b` dan `max=ip[b]` berfungsi untuk mengupdate IP yang jumlahnya paling banyak.

Hasil:
```
IP yang paling banyak mengakses server adalah: "" sebanyak ... requests
```

### Jawaban Soal 2d

Pada soal 2d, diminta untuk menampilkan request dengan menggunakan user-agent curl. Pertama, kami menggunakan command BEGIN untuk mendeklarasikan field separator (FS) dan `n`.
```shell
awk 'BEGIN {FS=":";n=0;}
```
Kemudian, kami memberikan action `/curl/` dan `++n` untuk cek apakah ada curl tiap baris, kemudian jika ada akan di count.
```shell
/curl/ {++n}
```
Selanjutnya, kami deklarasikan command END, kemudian print `n` yang merupakan banyak request yang menggunakan user-agent curl dan hasil dimasukkan ke dalam result.txt.
```shell
END {
  print "Ada", n, "requests yang menggunakan curl sebagai user-agent\n"
}
 ' log_website_daffainfo.log >> "$root"/forensic_log_website_daffainfo_log/result.txt
```

### Jawaban Soal 2e

Pada soal 2e, diminta untuk menampilkan request yang dilakukan pada tanggal 22 di jam 2. Pertama, kami gunakan command BEGIN untuk mendeklarasikan field separator (FS).
```shell
awk 'BEGIN {FS=":";}
```
Kemudian, kami beri action untuk mencari string yang memuat 22/Jan/2022:02 dan diletakkan ke dalam array ip, lalu dilakukan increment.
```shell
if(NR>1 && $2~"22/Jan/2022:02"){
  ip[$1]=ip[$1]+1
 }
```
Selanjutnya, kami deklarasikan END dan print IP yang telah disimpan di array ip.
```shell
END {
  for(hasil in ip) printf("%s\n", hasil)
}
 ' log_website_daffainfo.log >> "$root"/forensic_log_website_daffainfo_log/result.txt
```

## Soal 3 (REVISI)
Menggunakan command touch untuk membuat file
```shell
filename="metrics_"$(date "+%Y%m%d%H%M%S")".log"
touch $filename
```
Redirect hasil yang didapat dari free -m ke file ram.log
```shell
free -m > ram.log
```
Ambil data di ram log sesuai indeks kolomnya
```shell
awk 'FNR==2{printf $2","$3","$4","$5","$6","$7","}''FNR==3{printf $2","$3","$4","}' ram.log >> ./log/$filename
```
Cari nama user dan append user location (pwd) ke file sebelumnya dengan menskip line
```shell
user=`pwd`
echo -n $user >> ./log/$filename
```
Move file dari du -sh $HOME ke file du.log
dengan awk, masukkan ke file tujuan
```shell
du -sh $HOME  > du.log
awk '{print ","$1}' du.log >> ./log/$filename
```
Ubah permission file tersebut supaya tidak bisa dibaca oleh user lain selain pembuat file itu
```shell
chmod 400 ~/log/$filename
```
Pakai crontjob di crontab -e supaya auto jalan file tersebut
```shell
* * * * * /home/ingwerludwig/minute_log.sh
```

aggregate_minutes_to_hourly_log.sh (REVISI)
Buat variable perHour untuk mengetahui dan menyimpan data satu jam yang lalu.
```shell
perHour=$(date -d "$x - 1 hours" "+%Y%m%d%H")
```
Gunakan chmod -R 777 untuk memungkinkan SEMUA pengguna dapat izin untuk membaca, menulis, dan mengeksekusi isi file di log.
```shell
chmod -R 777 /home/$USER/log/
```
Bandingkan setiap nilai di metrics dengan variable pembanding (100000) kemudian gunakan awk yang sesuai waktu dan dimasukkan ke file temp beranam temp.log
```shell
 awk -F"," '
BEGIN{OFS=",";MIN1=100000;MIN2=100000;MIN3=100000;MIN4=100000;MIN5=100000;MIN6=100000;MIN7=100000;MIN8=100000;MIN9=100000;MIN11=100000}
        $1<MIN1 {MIN1=$1;} $2<MIN2 {MIN2=$2;} $3<MIN3 {MIN3=$3;} $4<MIN4 {MIN4=$4;} $5<MIN5 {MIN5=$5;} $6<MIN6 {MIN6=$6;} $7<MIN7 {MIN7=$7;} $8<MIN8 {MIN8=$8;} $9<MIN9 {MIN9=$9;} $11<MIN11 {MIN11=$11;}
END{print "minimum",MIN1,MIN2,MIN3,MIN4,MIN5,MIN6,MIN7,MIN8,MIN9,$10,MIN11}' ~/log/metrics_$perHour*.log > temp.log
```
Bandingkan setiap nilai di metrics dengan variable pembanding (0) kemudian gunakan awk yang sesuai waktu dan dimasukkan ke file temp beranam temp.log
Gunakan FNR%2==0 untuk memastikan kalau line yang dibaca hanya line 2
```shell
 awk -F"," '
 awk -F"," '
BEGIN{OFS=",";MAX1=0;MAX2=0;MAX3=0;MAX4=0;MAX5=0;MAX6=0;MAX7=0;MAX8=0;MAX9=0;MAX1=0}
        $1>MAX1 && FNR%2==0{MAX1=$1;} $2>MAX2 && FNR%2==0{MAX2=$2;} $3>MAX3 && FNR%2==0{MAX3=$3;} $4>MAX4 &&FNR%2==0 {MAX4=$4;} $5>MAX5 && FNR%2==0{MAX5=$5;} $6>MAX6 && FNR%2==0{MAX6=$6;} $7>MAX7 &&FNR%2==0{MAX7=$7;} $8>MAX8 && FNR%2==0{MAX8=$8;} $9>MAX9 &&FNR%2==0{MAX9=$9;} $11>MAX11 && FNR%2==0{MAX11=$11;}
END{print "maximum",MAX1,MAX2,MAX3,MAX4,MAX5,MAX6,MAX7,MAX8,MAX9,$10,MAX11}' ~/log/metrics_$perHour*.log >> temp.log
```
Untuk mendapatkan nilai rata-rata, jumlahkan tiap variable kemudian dibagi dengan jumlah line yang ada dan disimpan ke temp.log
```shell
awk -F"," '
BEGIN{OFS=",";A=0;B=0;C=0;D=0;E=0;F=0;G=0;H=0;I=0;K=0}
	{A+=$1;B+=$2;C+=$3;D+=$4;E+=$5;F+=$6;G+=$7;H+=$8;I+=$9;K+=$11}
END{print "average",A/NR,B/NR,C/NR,D/NR,E/NR,F/NR,G/NR,H/NR,I/NR,$10,K/NR"G"}' ~/log/metrics_$perHour*.log >> temp.log
```
Inisiasi nama file
```shell
string="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available_swap_used,swap_free,path,path_size"
```
Masukkan string ke file metrics_agg_$perHour.log dan redirect file yang ada di temp.log ke metrics_agg_$perHour.log.
echo $string > /home/ingwerludwig/log/metrics_agg_$perHour.log && cat /home/ingwerludwig/temp.log >> /home/ingwerludwig/log/metrics_agg_$perHour.log
```shell
echo $string > /home/ingwerludwig/log/metrics_agg_$perHour.log && cat /home/ingwerludwig/temp.log >> /home/ingwerludwig/log/metrics_agg_$perHour.log
```
Ubah permission agar file hanya dapat dilihat oleh user yang membuat file tersebut
```shell
chmod 400 /home/$USER/log/metrics_agg_$perHour.log
```
Tambahin crontjob di crontab -e agar file ke run otomatis
```shell
00 * * * * /home/ingwerludwig/aggregate_minutes_to_hourly_log.sh 
```
Kendala : Tidak bisa run free -m di MAC OS, nilai metrics max tidak keluar, cron salah
Solusi :
1. Pakai FNR%2==0 supaya hanya line 2 yang dibaca
2. Memakai laptop teman yang OS nya linux
3. Menulis "G" supaya ada satuannya

