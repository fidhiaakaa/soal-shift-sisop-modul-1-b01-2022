#!/bin/bash

name='metrics_'
dt=$(date '+%Y%m%d%H%M%S');
filename="${name}${dt}"
touch $filename.log

FREE_BLOCKS=$(vm_stat | grep free | awk '{ print $3 }' | sed 's/\.//')
INACTIVE_BLOCKS=$(vm_stat | grep inactive | awk '{ print $3 }' | sed 's/\.//')
SPECULATIVE_BLOCKS=$(vm_stat | grep speculative | awk '{ print $3 }' | sed 's/\.//')

FREE=$((($FREE_BLOCKS+SPECULATIVE_BLOCKS)*4096/1048576))
INACTIVE=$(($INACTIVE_BLOCKS*4096/1048576))
TOTAL=$((($FREE+$INACTIVE)))
echo Free RAM MAC :       $FREE MB >> $filename.log
echo Inactive RAM MAC :   $INACTIVE MB >> $filename.log
echo Total free RAM MAC : $TOTAL MB >> $filename.log
